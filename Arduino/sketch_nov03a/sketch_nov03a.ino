union easycast {
  float f;
  int32_t i;
  char b[sizeof(float)];
};

easycast temp;

void setup() {
  Serial.begin(115200);
  temp.f = 3.14;

}

void loop() {
  Serial.println((uint8_t)temp.b[0]);
  Serial.println(temp.b[1]);
  Serial.println(temp.b[2]);
  Serial.println(temp.b[3]);
  Serial.println("------------------");
}
