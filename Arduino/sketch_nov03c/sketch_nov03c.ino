/* INCLUDE */
#include <RingBuf.h>

/* DEFINE */
#define BAUDRATE 115200

#define N 14
#define buffer_length (2+(N * 4)+(N - 1))

/* TYPE DEFINITION */
union easycast {
  float f;
  int32_t i;
  uint8_t b[sizeof(float)];
};

/* VARIABLES */
const byte numChars = buffer_length;
char receivedChars[numChars];
char tempChars[numChars];        // temporary array for use when parsing
boolean newData = false;

/*** TIME CONSTRAINTS ***/
unsigned long t;
unsigned long period = 100;

/*** DRONE STATE INTERNAL ***/

/*** DRONE STATE IN ***/
float roll_ref = 0;
float roll_kp = 0;
float roll_ki = 0;
float roll_kd = 0;

float pitch_ref = 0;
float pitch_kp = 0;
float pitch_ki = 0;
float pitch_kd = 0;

float yaw_ref = 0;
float yaw_kp = 0;
float yaw_ki = 0;
float yaw_kd = 0;

float ref_u = 0;
int drone_status_on = 0;

/*** DRONE STATE OUT ***/
float roll_ang = 0;
float roll_dang = 0;

float pitch_ang = 0;
float pitch_dang = 0;

float yaw_ang = 0;
float yaw_dang = 0;

float M1 = 0;
float M2 = 0;
float M3 = 0;
float M4 = 0;

/* FUNCTION DEFINITION */


/* FUNCTIONS */
void setup() {
  // Adjust baudrate here
  Serial.begin(BAUDRATE);
  //while (!Serial) {}



  t = millis();
}

void loop() {

  if (millis() > t) {
    t += period;
    M1 = M2 = M3 = M4 = ref_u;
    Serial.print('{');
    Serial.print(roll_ref);
    Serial.print(',');
    Serial.print(pitch_ref);
    Serial.print(',');
    Serial.print(yaw_ref);
    Serial.print(',');
    Serial.print(roll_dang);
    Serial.print(',');
    Serial.print(pitch_dang);
    Serial.print(',');
    Serial.print(yaw_dang);
    Serial.print(',');
    Serial.print(M1);
    Serial.print(',');
    Serial.print(M2);
    Serial.print(',');
    Serial.print(M3);
    Serial.print(',');
    Serial.print(M4);
    Serial.print('}');
  }
  recvWithStartEndMarkers();
  if (newData == true) {
    strcpy(tempChars, receivedChars);
    // this temporary copy is necessary to protect the original data
    //   because strtok() used in parseData() replaces the commas with \0
    parseData();
    newData = false;
  }
}

//============

void recvWithStartEndMarkers() {
  static boolean recvInProgress = false;
  static byte ndx = 0;
  char startMarker = '{';
  char endMarker = '}';
  char rc;

  while (Serial.available() > 0 && newData == false) {
    rc = Serial.read();

    if (recvInProgress == true) {
      if (rc != endMarker) {
        receivedChars[ndx] = rc;
        ndx++;
        if (ndx >= numChars) {
          ndx = numChars - 1;
        }
      }
      else {
        receivedChars[ndx] = '\0'; // terminate the string
        recvInProgress = false;
        ndx = 0;
        newData = true;
      }
    }

    else if (rc == startMarker) {
      recvInProgress = true;
    }
  }
}

//============

void parseData() {      // split the data into its parts

  char * strtokIndx; // this is used by strtok() as an index

  strtokIndx = strtok(tempChars, ",");     // get the first part - the string
  roll_ref = atof(strtokIndx);     // convert this part to a float

  strtokIndx = strtok(NULL, ",");     // this continues where the previous call left off
  roll_kp = atof(strtokIndx);     // convert this part to a float

  strtokIndx = strtok(NULL, ",");     // this continues where the previous call left off
  roll_ki = atof(strtokIndx);     // convert this part to a float

  strtokIndx = strtok(NULL, ",");     // this continues where the previous call left off
  roll_kd = atof(strtokIndx);     // convert this part to a float

  strtokIndx = strtok(NULL, ",");     // this continues where the previous call left off
  pitch_ref = atof(strtokIndx);     // convert this part to a float

  strtokIndx = strtok(NULL, ",");     // this continues where the previous call left off
  pitch_kp = atof(strtokIndx);     // convert this part to a float

  strtokIndx = strtok(NULL, ",");     // this continues where the previous call left off
  pitch_ki = atof(strtokIndx);     // convert this part to a float

  strtokIndx = strtok(NULL, ",");     // this continues where the previous call left off
  pitch_kd = atof(strtokIndx);     // convert this part to a float

  strtokIndx = strtok(NULL, ",");     // this continues where the previous call left off
  yaw_ref = atof(strtokIndx);     // convert this part to a float

  strtokIndx = strtok(NULL, ",");     // this continues where the previous call left off
  yaw_kp = atof(strtokIndx);     // convert this part to a float

  strtokIndx = strtok(NULL, ",");     // this continues where the previous call left off
  yaw_ki = atof(strtokIndx);     // convert this part to a float

  strtokIndx = strtok(NULL, ",");     // this continues where the previous call left off
  yaw_kd = atof(strtokIndx);     // convert this part to a float

  strtokIndx = strtok(NULL, ",");     // this continues where the previous call left off
  ref_u = atof(strtokIndx);     // convert this part to a float

  strtokIndx = strtok(NULL, ","); // this continues where the previous call left off
  drone_status_on = atoi(strtokIndx);     // convert this part to an integer

}
