/* INCLUDE */
#include <RingBuf.h>

/* DEFINE */
#define BAUDRATE 115200

#define N 14
#define buffer_length (2+(N * 4)+(N - 1))

/* TYPE DEFINITION */
union easycast {
  float f;
  int32_t i;
  uint8_t b[sizeof(float)];
};

/* VARIABLES */
easycast temp;

enum packet_state {SYNC, READ, WAIT};
bool havePacket = false;
easycast tempPacket[N];
int read_idx = 0;
packet_state read_status;
RingBuf<uint8_t, buffer_length> serial_buffer;
unsigned long packet_timestamp;
uint8_t temp_read_value;

/*** TIME CONSTRAINTS ***/
unsigned long t;
unsigned long period = 100;

/*** DRONE STATE INTERNAL ***/

/*** DRONE STATE IN ***/
float roll_ref = 0;
float roll_kp = 0;
float roll_ki = 0;
float roll_kd = 0;

float pitch_ref = 0;
float pitch_kp = 0;
float pitch_ki = 0;
float pitch_kd = 0;

float yaw_ref = 0;
float yaw_kp = 0;
float yaw_ki = 0;
float yaw_kd = 0;

float ref_u = 0;
int drone_status_on = 0;

/*** DRONE STATE OUT ***/
float roll_ang = 0;
float roll_dang = 0;

float pitch_ang = 0;
float pitch_dang = 0;

float yaw_ang = 0;
float yaw_dang = 0;

float M1 = 0;
float M2 = 0;
float M3 = 0;
float M4 = 0;

/* FUNCTION DEFINITION */


/* FUNCTIONS */
void setup() {
  // Adjust baudrate here
  Serial.begin(BAUDRATE);
  //while (!Serial) {}

  read_status = SYNC;

  t = millis();
}

void loop() {

  if (millis() > t) {
    t += period;
    M1 = M2 = M3 = M4 = ref_u;
    Serial.print('{');
    Serial.print(tempPacket[1].f);
    Serial.print(',');
    Serial.print(pitch_ang);
    Serial.print(',');
    Serial.print(yaw_ang);
    Serial.print(',');
    Serial.print(roll_dang);
    Serial.print(',');
    Serial.print(pitch_dang);
    Serial.print(',');
    Serial.print(yaw_dang);
    Serial.print(',');
    Serial.print(M1);
    Serial.print(',');
    Serial.print(M2);
    Serial.print(',');
    Serial.print(M3);
    Serial.print(',');
    Serial.print(M4);
    Serial.print('}');
  }
  
  recvWithStartEndMarkers();
  if (newData == true) {
      strcpy(tempChars, receivedChars);
          // this temporary copy is necessary to protect the original data
          //   because strtok() used in parseData() replaces the commas with \0
      parseData();
      showParsedData();
      newData = false;
  }
}

void serial_decoder() {
  switch (read_status) {
    case SYNC:
      if (serial_buffer.size() > 0) serial_buffer.pop(temp_read_value);
      if (temp_read_value == '{') {
        read_status = READ;
        read_idx = 0;
        temp_read_value = 0;
      }
      break;
    case READ:
      if (serial_buffer.size() >= 5) {
        if (serial_buffer[4] == ',' || serial_buffer[4] == '}') {
          serial_buffer.pop(temp_read_value);
          temp.b[0] = temp_read_value;
          serial_buffer.pop(temp_read_value);
          temp.b[1] = temp_read_value;
          serial_buffer.pop(temp_read_value);
          temp.b[2] = temp_read_value;
          serial_buffer.pop(temp_read_value);
          temp.b[3] = temp_read_value;
          tempPacket[read_idx].f = temp.f;
          read_idx++;
          serial_buffer.pop(temp_read_value);
          if (serial_buffer[4] == '}') {
            read_status = WAIT;
            havePacket = true;
            packet_timestamp = millis();
          }
        }
                  else {
                    read_status = SYNC;
                  }
      }
      break;
    case WAIT:
      if (havePacket == false) read_status = SYNC;
      break;
  }
}
