N = 10;
datain_base = datain;
datalength = length(datain)/N;
datain = zeros(datalength,N);
for i=1:datalength-1
   datain(i:i+N-1) = datain_base(1:N)';
end